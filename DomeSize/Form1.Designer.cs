﻿namespace DomeSize
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.rulerList = new System.Windows.Forms.ComboBox();
            this.calcNumText = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.clearBtn = new System.Windows.Forms.Button();
            this.calcBtn = new System.Windows.Forms.Button();
            this.rulerNameLabel = new System.Windows.Forms.Label();
            this.rulerSizeNumLabel = new System.Windows.Forms.Label();
            this.rulerUnitLabel = new System.Windows.Forms.Label();
            this.resultNumLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.imagePanel = new System.Windows.Forms.FlowLayoutPanel();
            this.SuspendLayout();
            // 
            // rulerList
            // 
            this.rulerList.FormattingEnabled = true;
            this.rulerList.Location = new System.Drawing.Point(12, 12);
            this.rulerList.Name = "rulerList";
            this.rulerList.Size = new System.Drawing.Size(265, 20);
            this.rulerList.TabIndex = 0;
            this.rulerList.SelectedIndexChanged += new System.EventHandler(this.RulerListChanged);
            // 
            // calcNumText
            // 
            this.calcNumText.Font = new System.Drawing.Font("MS UI Gothic", 22F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.calcNumText.Location = new System.Drawing.Point(13, 73);
            this.calcNumText.Name = "calcNumText";
            this.calcNumText.Size = new System.Drawing.Size(264, 37);
            this.calcNumText.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Mongolian Baiti", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(13, 126);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(62, 61);
            this.button1.TabIndex = 2;
            this.button1.Text = "1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.NumButtonClick);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Mongolian Baiti", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(81, 126);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(62, 61);
            this.button2.TabIndex = 3;
            this.button2.Text = "2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.NumButtonClick);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Mongolian Baiti", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(149, 126);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(62, 61);
            this.button3.TabIndex = 4;
            this.button3.Text = "3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.NumButtonClick);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Mongolian Baiti", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(12, 193);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(62, 61);
            this.button4.TabIndex = 5;
            this.button4.Text = "4";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.NumButtonClick);
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Mongolian Baiti", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(81, 194);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(62, 60);
            this.button5.TabIndex = 6;
            this.button5.Text = "5";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.NumButtonClick);
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Mongolian Baiti", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(149, 193);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(62, 61);
            this.button6.TabIndex = 7;
            this.button6.Text = "6";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.NumButtonClick);
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("Mongolian Baiti", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Location = new System.Drawing.Point(12, 260);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(62, 61);
            this.button7.TabIndex = 8;
            this.button7.Text = "7";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.NumButtonClick);
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("Mongolian Baiti", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.Location = new System.Drawing.Point(82, 260);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(61, 61);
            this.button8.TabIndex = 9;
            this.button8.Text = "8";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.NumButtonClick);
            // 
            // button9
            // 
            this.button9.Font = new System.Drawing.Font("Mongolian Baiti", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.Location = new System.Drawing.Point(150, 261);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(61, 60);
            this.button9.TabIndex = 10;
            this.button9.Text = "9";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.NumButtonClick);
            // 
            // button10
            // 
            this.button10.Font = new System.Drawing.Font("Mongolian Baiti", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button10.Location = new System.Drawing.Point(12, 327);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(61, 61);
            this.button10.TabIndex = 11;
            this.button10.Text = "0";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.NumButtonClick);
            // 
            // button11
            // 
            this.button11.Font = new System.Drawing.Font("Mongolian Baiti", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.Location = new System.Drawing.Point(82, 326);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(61, 61);
            this.button11.TabIndex = 12;
            this.button11.Text = "00";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.NumButtonClick);
            // 
            // button12
            // 
            this.button12.Font = new System.Drawing.Font("Mongolian Baiti", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.Location = new System.Drawing.Point(150, 326);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(62, 61);
            this.button12.TabIndex = 13;
            this.button12.Text = "000";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.NumButtonClick);
            // 
            // clearBtn
            // 
            this.clearBtn.BackColor = System.Drawing.SystemColors.Info;
            this.clearBtn.Font = new System.Drawing.Font("Mongolian Baiti", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clearBtn.Location = new System.Drawing.Point(217, 126);
            this.clearBtn.Name = "clearBtn";
            this.clearBtn.Size = new System.Drawing.Size(61, 61);
            this.clearBtn.TabIndex = 14;
            this.clearBtn.Text = "C";
            this.clearBtn.UseVisualStyleBackColor = false;
            this.clearBtn.Click += new System.EventHandler(this.ClearBtnClick);
            // 
            // calcBtn
            // 
            this.calcBtn.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.calcBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.calcBtn.Font = new System.Drawing.Font("Mongolian Baiti", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calcBtn.Location = new System.Drawing.Point(217, 261);
            this.calcBtn.Name = "calcBtn";
            this.calcBtn.Size = new System.Drawing.Size(62, 127);
            this.calcBtn.TabIndex = 15;
            this.calcBtn.Text = "計算";
            this.calcBtn.UseVisualStyleBackColor = false;
            this.calcBtn.Click += new System.EventHandler(this.CalcBtnClick);
            // 
            // rulerNameLabel
            // 
            this.rulerNameLabel.AutoSize = true;
            this.rulerNameLabel.Font = new System.Drawing.Font("MS UI Gothic", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rulerNameLabel.Location = new System.Drawing.Point(361, 268);
            this.rulerNameLabel.Name = "rulerNameLabel";
            this.rulerNameLabel.Size = new System.Drawing.Size(159, 33);
            this.rulerNameLabel.TabIndex = 16;
            this.rulerNameLabel.Text = "東京ドーム";
            // 
            // rulerSizeNumLabel
            // 
            this.rulerSizeNumLabel.AutoSize = true;
            this.rulerSizeNumLabel.Font = new System.Drawing.Font("MS UI Gothic", 19F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rulerSizeNumLabel.Location = new System.Drawing.Point(372, 327);
            this.rulerSizeNumLabel.Name = "rulerSizeNumLabel";
            this.rulerSizeNumLabel.Size = new System.Drawing.Size(51, 26);
            this.rulerSizeNumLabel.TabIndex = 17;
            this.rulerSizeNumLabel.Text = "???";
            // 
            // rulerUnitLabel
            // 
            this.rulerUnitLabel.AutoSize = true;
            this.rulerUnitLabel.Font = new System.Drawing.Font("MS UI Gothic", 19F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rulerUnitLabel.Location = new System.Drawing.Point(498, 326);
            this.rulerUnitLabel.Name = "rulerUnitLabel";
            this.rulerUnitLabel.Size = new System.Drawing.Size(39, 26);
            this.rulerUnitLabel.TabIndex = 18;
            this.rulerUnitLabel.Text = "㎡";
            // 
            // resultNumLabel
            // 
            this.resultNumLabel.AutoSize = true;
            this.resultNumLabel.Font = new System.Drawing.Font("MS UI Gothic", 26F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.resultNumLabel.Location = new System.Drawing.Point(612, 305);
            this.resultNumLabel.Name = "resultNumLabel";
            this.resultNumLabel.Size = new System.Drawing.Size(49, 35);
            this.resultNumLabel.TabIndex = 19;
            this.resultNumLabel.Text = "??";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 18.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(333, 327);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 25);
            this.label1.TabIndex = 21;
            this.label1.Text = "(";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS UI Gothic", 18.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(563, 326);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(21, 25);
            this.label2.TabIndex = 22;
            this.label2.Text = ")";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS UI Gothic", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(697, 305);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 35);
            this.label3.TabIndex = 23;
            this.label3.Text = "個　分";
            // 
            // imagePanel
            // 
            this.imagePanel.Location = new System.Drawing.Point(304, 14);
            this.imagePanel.Name = "imagePanel";
            this.imagePanel.Size = new System.Drawing.Size(480, 240);
            this.imagePanel.TabIndex = 24;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(822, 399);
            this.Controls.Add(this.imagePanel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.resultNumLabel);
            this.Controls.Add(this.rulerUnitLabel);
            this.Controls.Add(this.rulerSizeNumLabel);
            this.Controls.Add(this.rulerNameLabel);
            this.Controls.Add(this.calcBtn);
            this.Controls.Add(this.clearBtn);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.calcNumText);
            this.Controls.Add(this.rulerList);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox rulerList;
        private System.Windows.Forms.TextBox calcNumText;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button clearBtn;
        private System.Windows.Forms.Button calcBtn;
        private System.Windows.Forms.Label rulerNameLabel;
        private System.Windows.Forms.Label rulerSizeNumLabel;
        private System.Windows.Forms.Label rulerUnitLabel;
        private System.Windows.Forms.Label resultNumLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.FlowLayoutPanel imagePanel;
    }
}

